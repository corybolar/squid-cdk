def regionchoices = ['us-east-1', 'us-west-2'].join('\n')

pipelineJob('main-job') {
  concurrentBuild(false)
  parameters {
    string {
        name 'ACCOUNT_ID'
        description 'Account ID to execute job in'
        defaultValue '934496811309'
        trim true
    }
    choice {
        name 'REGION'
        description 'AWS region to create stack in'
        choices regionchoices
    }
    string {
        name 'ROLE_NAME'
        description 'Role name to use to cross'
        defaultValue 'aws-cbsbx2-admin'
        trim true
    }
    credentials {
        name 'AWS_CREDENTIALS'
        description 'AWS Credentials to use for build'
        defaultValue 'cb-sbx-2'
        credentialType 'com.cloudbees.jenkins.plugins.awscredentials'
        required true
    }
    string {
        name 'VPC_NAME'
        description 'VPC Name'
        defaultValue 'sandbox'
        trim true
    }
    string {
        name 'PROXY_FROM_LAYER'
        description 'VPC subnet layer to proxy traffic FROM'
        defaultValue 'app'
        trim true
    }
    string {
        name 'PROXY_TO_LAYER'
        description 'VPC subnet layer to proxy traffic TO'
        defaultValue 'web'
        trim true
    }
    string {
        name 'KEY_NAME'
        description 'AWS SSH Key name'
        defaultValue 'cory@Arch-2017-08-01'
        trim true
    }
    string {
        name 'SOURCE_BRANCH'
        description 'Git branch to build from'
        defaultValue 'master'
        trim true
    }
    booleanParam {
        name 'REFRESH_CONTEXT'
        defaultValue false
        description "CAREFUL: Setting this to true causes cdk context to refresh. This will likely result in new squid instances as AMI will change"
    }
  }
  logRotator {
    numToKeep 5
    artifactNumToKeep 1
  }
  definition {
    cpsScm {
      scm {
        git {
          branch '*/${SOURCE_BRANCH}'
          remote {
            url('git@gitlab.com:cbpeckles/squid-cdk.git')
            credentials('gitlab')
          }
          extensions {
            wipeWorkspace()
          }
        }
        scriptPath('./jenkins/jenkinsfiles/mainjob.groovy')
      }
    }
  }
}
