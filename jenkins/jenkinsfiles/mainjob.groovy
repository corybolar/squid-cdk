pipeline {
    agent any
    stages {
        stage('Install aws cdk') {
            steps{
                nvm(version:'v12.13.1') {
                    sh '''
                    node -v
                    npm i -g aws-cdk@1.19.0
                    cdk --version
                    '''
                    withPythonEnv('python3.7') {
                        sh '''
                        which python
                        python --version
                        pip install -r requirements.txt
                        '''
                        withCredentials([[$class: 'AmazonWebServicesCredentialsBinding', credentialsId: '${AWS_CREDENTIALS}']]) {
                            script {
                                sh '''
                                #!/bin/bash
                                echo $SHELL
                                export AWS_DEFAULT_REGION=${REGION}
                                set +x
                                CREDS=$(aws sts assume-role --role-arn "arn:aws:iam::${ACCOUNT_ID}:role/${ROLE_NAME}" --role-session-name "jenkins-squid-cdk-${BUILD_NUMBER}" --output text)
                                export AWS_ACCESS_KEY_ID=$(echo $CREDS | cut -d' ' -f5)
                                export AWS_SECRET_ACCESS_KEY=$(echo $CREDS | cut -d' ' -f7)
                                export AWS_SESSION_TOKEN=$(echo $CREDS | cut -d' ' -f8)
                                set -x
                                export CDK_DEFAULT_ACCOUNT=${ACCOUNT_ID}
                                export CDK_DEFAULT_REGION=${REGION}
                                # Get old context if it exists
                                PARAMETERS=$(aws ssm describe-parameters --filters Key=Name,Values="/ccs/squid/${VPC_NAME}/${PROXY_FROM_LAYER}/context" --query Parameters --output text)
                                if [ "${REFRESH_CONTEXT}" = false ] && [ ! -z "$PARAMETERS" ]; then
                                echo "Existing context found!"
                                aws ssm get-parameter --name "/ccs/squid/${VPC_NAME}/${PROXY_FROM_LAYER}/context" --query Parameter.Value --output text | tee cdk.context.json
                                else
                                cdk context --clear
                                echo "No existing context"
                                fi
                                cdk synth --context vpc_name=${VPC_NAME} --context proxy_from_layer=${PROXY_FROM_LAYER} --context proxy_to_layer=${PROXY_TO_LAYER} --context keyname=${KEY_NAME}
                                cdk diff --context vpc_name=${VPC_NAME} --context proxy_from_layer=${PROXY_FROM_LAYER} --context proxy_to_layer=${PROXY_TO_LAYER} --context keyname=${KEY_NAME} || true
                                '''
                                // env.STSCREDS = sh(returnStdout: true, script: 'aws sts assume-role --role-arn "arn:aws:iam::${ACCOUNT_ID}:role/${ROLE_NAME} --role-session-name "jenkins-squid-cdk-${BUILD_NUMBER}"')
                                // echo "${env.STSCREDS}"
                            }
                        }
                    }
                }
            }
        }
        stage('Approval') {
            steps {
                input message: "Proceed with deployment?"
            }
        }
        stage('Deploy') {
            steps {
                nvm(version:'v12.13.1') {
                    sh '''
                    node -v
                    npm i -g aws-cdk@1.19.0
                    cdk --version
                    '''
                    withPythonEnv('python3.7') {
                        sh '''
                        which python
                        python --version
                        pip install -r requirements.txt
                        '''
                        withCredentials([[$class: 'AmazonWebServicesCredentialsBinding', credentialsId: '${AWS_CREDENTIALS}']]) {
                            script {
                                sh '''
                                #!/bin/bash
                                export AWS_DEFAULT_REGION=${REGION}
                                set +x
                                CREDS=$(aws sts assume-role --role-arn "arn:aws:iam::${ACCOUNT_ID}:role/${ROLE_NAME}" --role-session-name "jenkins-squid-cdk-${BUILD_NUMBER}" --output text)
                                export AWS_ACCESS_KEY_ID=$(echo $CREDS | cut -d' ' -f5)
                                export AWS_SECRET_ACCESS_KEY=$(echo $CREDS | cut -d' ' -f7)
                                export AWS_SESSION_TOKEN=$(echo $CREDS | cut -d' ' -f8)
                                set -x
                                export CDK_DEFAULT_ACCOUNT=${ACCOUNT_ID}
                                export CDK_DEFAULT_REGION=${REGION}
                                # Get old context if it exists
                                PARAMETERS=$(aws ssm describe-parameters --filters Key=Name,Values="/ccs/squid/${VPC_NAME}/${PROXY_FROM_LAYER}/context" --query Parameters --output text)
                                if [ "${REFRESH_CONTEXT}" = false ] && [ ! -z "$PARAMETERS" ]; then
                                echo "Existing context found!"
                                aws ssm get-parameter --name "/ccs/squid/${VPC_NAME}/${PROXY_FROM_LAYER}/context" --query Parameter.Value --output text | tee cdk.context.json
                                else
                                cdk context --clear
                                echo "No existing context"
                                fi
                                cdk synth --context vpc_name=${VPC_NAME} --context proxy_from_layer=${PROXY_FROM_LAYER} --context proxy_to_layer=${PROXY_TO_LAYER} --context keyname=${KEY_NAME}
                                cdk diff --context vpc_name=${VPC_NAME} --context proxy_from_layer=${PROXY_FROM_LAYER} --context proxy_to_layer=${PROXY_TO_LAYER} --context keyname=${KEY_NAME} || true
                                cdk deploy --require-approval never --context vpc_name=${VPC_NAME} --context proxy_from_layer=${PROXY_FROM_LAYER} --context proxy_to_layer=${PROXY_TO_LAYER} --context keyname=${KEY_NAME} || true
                                '''
                            }
                        }
                    }
                }
            }
        }
    }
    post {
        cleanup {
            cleanWs(
                deleteDirs: true,
                disableDeferredWipeout: true
            )
        }
    }
}
