#!/usr/bin/env python3

from aws_cdk import core
from squid_proxy.squid_proxy_stack import SquidProxyStack
import os

app = core.App()

env_Deployment = core.Environment(account=os.environ['CDK_DEFAULT_ACCOUNT'], region=os.environ['CDK_DEFAULT_REGION'])
stackname=str(
    "squid-proxy"+"-"+
    app.node.try_get_context('vpc_name')+"-"+
    app.node.try_get_context('proxy_from_layer')+"-"+
    app.node.try_get_context('proxy_to_layer')
)
stack = SquidProxyStack(app, stackname, env=env_Deployment)

app.synth()
