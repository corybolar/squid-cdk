from aws_cdk import (
    aws_cloudformation as cfn,
    aws_lambda as lambda_,
    aws_iam as iam,
    core
)

class RouteChangeResource(core.Construct):
    def __init__(self, scope: core.Construct, id: str, **kwargs) -> None:
        super().__init__(scope, id)

        with open("squid_proxy/route-change-handler.py", encoding="utf-8") as fp:
            code_body = fp.read()

        lambdafunction = lambda_.SingletonFunction(
            self, "Singleton",
            uuid="b78940b7-fe68-4d27-8a4d-40d2ca30e3ee",
            code=lambda_.InlineCode(code_body),
            handler="index.main",
            timeout=core.Duration.seconds(90),
            runtime=lambda_.Runtime.PYTHON_3_7,
        )
        lambdafunction.add_to_role_policy(
            iam.PolicyStatement(
                actions=[
                    "ec2:ReplaceRoute",
                    "ec2:CreateRoute",
                    "ec2:DeleteRoute",
                ],
                resources=["arn:"+core.Aws.PARTITION+":ec2:"+core.Aws.REGION+":"+core.Aws.ACCOUNT_ID+":route-table/"+rtbids for rtbids in kwargs['RouteTableIds']]
            )
        )
        lambdafunction.add_to_role_policy(
            iam.PolicyStatement(
                actions=[
                    "ec2:Get*",
                    "ec2:Describe*",
                    "ec2:List*",
                ],
                resources=[
                    "*"
                ]
            )
        )
        resource = cfn.CustomResource(
            self, "Resource",
            provider=cfn.CustomResourceProvider.from_lambda(
                handler=lambdafunction,
            ),
            properties=kwargs,
        )
        resource.node.add_dependency(lambdafunction)

        self.response = resource.get_att("Response").to_string()