from aws_cdk import (
    aws_ec2 as ec2,
    aws_ssm as ssm,
    aws_iam as iam,
    aws_cloudwatch as cw,
    aws_cloudformation as cfn,
    core
)
from aws_cdk.core import Tag
from squid_proxy.route_change_resource import RouteChangeResource
import re
import random

class CloudwatchRecoveryAction(cw.IAlarmAction):
    def bind(scope, alarm):
        return cw.AlarmActionConfig(alarm_action_arn = "arn:aws:automate:us-east-1:ec2:recover")
    

class SquidProxyStack(core.Stack):

    def __init__(self, scope: core.Construct, id: str, **kwargs) -> None:
        super().__init__(scope, id, **kwargs)

        # The code that defines your stack goes here
        vpc = ec2.Vpc.from_lookup(
            self,
            "vpc",
            vpc_name=self.node.try_get_context("vpc_name"),
            subnet_group_name_tag="Layer"
        )
        vid = vpc.vpc_id
        privproxytosubnets = vpc.select_subnets(
            one_per_az=True,
            subnet_group_name=self.node.try_get_context("proxy_to_layer"),
        )
        self.privproxyfromsubnets = vpc.select_subnets(
            one_per_az=True,
            subnet_group_name=self.node.try_get_context("proxy_from_layer"),
        )
        # Needed to work around the cdk doing multiple lookups on first stack call
        if not privproxytosubnets.subnets:
            print('No private proxy subnets with tags Layer -> ' + self.node.try_get_context("proxy_to_layer"))
            return
        if not self.privproxyfromsubnets.subnets:
            print('No private proxy subnets with tags Layer -> ' + self.node.try_get_context("proxy_from_layer"))
            return
        squidconf = open("./squid.conf", "rb").read()
        squidconf = str(squidconf, 'utf-8')
        squidconfssm = ssm.StringParameter(
            self,
            "squidconf",
            string_value=squidconf,
            description="Configuration of squid proxies",
            parameter_name="/ccs/squid/"+self.node.try_get_context("vpc_name")+"/" +
            self.node.try_get_context("proxy_from_layer")+"/conf"
        )
        squidwhitelist = open("./whitelist.txt", "rb").read()
        squidwhitelist = str(squidwhitelist, 'utf-8')
        squidwhitelistssm = ssm.StringParameter(
            self,
            "squidwhitelist",
            string_value=squidwhitelist,
            description="Whitelisted domains for squid proxies",
            parameter_name="/ccs/squid/"+self.node.try_get_context("vpc_name")+"/" +
            self.node.try_get_context("proxy_from_layer")+"/whitelist"
        )
        contextfile = open("./cdk.context.json", "r").read()
        #contextfile = str(contextfile, 'utf-8')
        contextfile = contextfile.strip()
        squidcdkcontextssm = ssm.StringParameter(
            self,
            "cdkcontextparam",
            string_value=contextfile,
            description="cdk.context.json file",
            parameter_name="/ccs/squid/"+self.node.try_get_context("vpc_name")+"/" +
            self.node.try_get_context("proxy_from_layer")+"/context"
        )

        data = open("./userdata.txt", "rb").read()
        data = str(data, 'utf-8')
        # These substitions are necessary to be able to seperate the userdata
        # from the rest of the codebase.  Better options for substitution may be
        # available in the future of CDK
        data = re.sub(
            r'\$\{SQUIDWHITELISTSSMNAME\}', 
            squidwhitelistssm.parameter_name, 
            data
        )
        data = re.sub(
            r'\$\{SQUIDCONFSSMNAME\}', 
            squidconfssm.parameter_name, 
            data
        )
        # datamime contains the code to execute userdata on every boot
        # this makes changes via cloudformation easier to deploy as they do not require new
        # EC2 instances be created on every userdata change
        datamime = open("./userdatamime.txt", "rb").read()
        datamime = str(datamime, 'utf-8')
        shell = ec2.UserData.custom(datamime)
        shell.add_commands(data)
        secgroup = ec2.SecurityGroup(
            self,
            "squidsecgroup",
            vpc=vpc,
            description="Allows traffic for Squid functionality",
        )
        secgroup.add_ingress_rule(
            ec2.Peer.ipv4("10.0.0.0/8"),
            ec2.Port.all_traffic()
        )
        squidrole = iam.Role(
            self,
            "squidrole",
            assumed_by=iam.ServicePrincipal(
                service="ec2",
            ),
            inline_policies={
                "ssmaccess": iam.PolicyDocument(
                    statements=[
                        iam.PolicyStatement(
                            actions=[
                                "ssm:GetParameter",
                            ],
                            resources=[
                                str("arn:"+core.Aws.PARTITION+":ssm:*:"+core.Aws.ACCOUNT_ID +
                                    ":parameter"+squidconfssm.parameter_name),
                                str("arn:"+core.Aws.PARTITION+":ssm:*:"+core.Aws.ACCOUNT_ID +
                                    ":parameter"+squidwhitelistssm.parameter_name),
                            ]
                        ),
                        iam.PolicyStatement(
                            actions=[
                                "ec2:DescribeTags",
                            ],
                            resources=[
                                "*",
                            ]
                        )
                    ]
                )
            }
        )
        self.defroutetargets = []
        eniattachments = []
        squidinstances = []
        for az in vpc.availability_zones:
            squideni = ec2.CfnNetworkInterface(
                self,
                "squideni"+az,
                subnet_id=[
                    sub for sub in self.privproxyfromsubnets.subnets if sub.availability_zone == az][0].subnet_id,
                description="squideni"+az,
                source_dest_check=False,
                group_set=[
                    secgroup.security_group_id
                ],
            )
            squid = ec2.Instance(
                self,
                "squid-proxy-v2-"+az,
                instance_type=ec2.InstanceType.of(
                    ec2.InstanceClass.BURSTABLE3,
                    ec2.InstanceSize.MICRO,
                ),
                # machine_image=ec2.AmazonLinuxImage(
                #     generation=ec2.AmazonLinuxGeneration.AMAZON_LINUX_2
                # ),
                machine_image=ec2.LookupMachineImage(
                    name="RHEL-7.7_HVM-20190923-x86_64-0-Hourly2-GP2",
                    owners=[
                        "309956199498"
                    ],
                ),
                vpc=vpc,
                key_name=self.node.try_get_context("keyname"),
                source_dest_check=False,
                security_group=secgroup,
                role=squidrole,
                vpc_subnets=ec2.SubnetSelection(
                    subnets=[
                        sub for sub in privproxytosubnets.subnets if sub.availability_zone == az],
                ),
            )
            squidinstances.append(squid)
            squideniattachment = ec2.CfnNetworkInterfaceAttachment(
                self,
                "squideniattach"+az,
                device_index='1',
                instance_id=squid.instance_id,
                network_interface_id=squideni.ref,
                delete_on_termination=False,
            )
            squideniattachment.add_depends_on(squid.node.default_child)
            #squid.node.default_child.cfn_options.creation_policy = core.CfnCreationPolicy(
            #    resource_signal=core.CfnResourceSignal(timeout="PT5M")
            #)
            shell2 = shell
            squid.add_user_data(shell2.render())
            squid.add_user_data(
                "/opt/aws/bin/cfn-signal -e $? --stack " + core.Aws.STACK_NAME + " --resource " +
                squid.node.default_child.logical_id + " --region " + core.Aws.REGION
            )
            defroute = {
                'target': squideni.ref,
                'routetableid': [sub for sub in self.privproxyfromsubnets.subnets if sub.availability_zone == az][0].route_table.route_table_id,
                'subnet': [sub for sub in self.privproxyfromsubnets.subnets if sub.availability_zone == az][0].subnet_id,
                'attachmentid': squideniattachment.ref,
                'vpc-id': vid,
                'az': az,
            }
            self.defroutetargets.append(defroute)
            eniattachments.append(squideniattachment)
            recovery = cw.Alarm(
                self,
                "squid-recovery-"+az,
                metric=cw.Metric(
                    metric_name="StatusCheckFailed_System",
                    namespace="AWS/EC2",
                    dimensions={
                        "InstanceId": squid.instance_id
                    },
                    period=core.Duration.minutes(1),
                    statistic="Maximum",
                ),
                evaluation_periods=5,
                threshold=1.0,
                actions_enabled=True,
                alarm_description=str(
                    "EC2 Auto Recovery for " + squid.instance_id),
                alarm_name=str("EC2-StatusChecks-" +
                               squid.instance_id+"-AutoRecovery"),
                datapoints_to_alarm=5,
                period=core.Duration.minutes(1),
                statistic="Maximum",
                treat_missing_data=cw.TreatMissingData.IGNORE,
            )
            # Broken CDK implemenation of alarm actions
            recovery.node.default_child.alarm_actions = [
                "arn:aws:automate:us-east-1:ec2:recover"
            ]

        #print(self.defroutetargets)
        resource = RouteChangeResource(
            self,
            "routechangelambdaresource",
            message="CustomResource test",
            RouteMappings=self.defroutetargets,
            RouteTableIds=[routetableid['routetableid']
                           for routetableid in self.defroutetargets]
        )
        core.CfnOutput(
            self, "ResponseMessage",
            description="The message that came back from the Custom Resource",
            value=resource.response,
        )
        for eniattachment in eniattachments:
            resource.node.add_dependency(eniattachment)
