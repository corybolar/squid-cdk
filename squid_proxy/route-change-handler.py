import boto3
import json
import logging as log
log.getLogger().setLevel(log.INFO)
ec2_client = boto3.client(service_name='ec2', region_name='us-east-1') 
def defroutetosquid(event: dict):
    try:
        routes = ec2_client.describe_route_tables(
            RouteTableIds=[
                event['routetableid']
            ]
        )['RouteTables'][0]['Routes']
        defroute = [route for route in routes if route['DestinationCidrBlock'] == '0.0.0.0/0']
        if defroute:
            res = ec2_client.replace_route(
                RouteTableId=event['routetableid'],
                DestinationCidrBlock='0.0.0.0/0',
                NetworkInterfaceId=event['target']
            )
        else:
            res = ec2_client.create_route(
                RouteTableId=event['routetableid'],
                DestinationCidrBlock='0.0.0.0/0',
                NetworkInterfaceId=event['target']
            )
        log.info(res)
    except Exception as e:
        log.critical(e)
        raise

def defroutetonat(event: dict):
    try:
        routes = ec2_client.describe_route_tables(
            RouteTableIds=[
                event['routetableid']
            ]
        )['RouteTables'][0]['Routes']
        defroute = [route for route in routes if route['DestinationCidrBlock'] == '0.0.0.0/0']
        natids = ec2_client.describe_nat_gateways(
            Filter=[
                {
                    'Name': 'vpc-id',
                    'Values': [
                        event['vpc-id']
                    ]
                }
            ]
        )['NatGateways']
        if not natids:
            log.info("Can't find NATs for this VPC. Removing routes to squid instances and bailing out")
            res = ec2_client.delete_route(
                RouteTableId=event['routetableid'],
                DestinationCidrBlock='0.0.0.0/0'
            )
            log.info(res)
            return
        for nat in natids:
            az = ec2_client.describe_subnets(
                SubnetIds=[
                    nat['SubnetId']
                ]
            )['Subnets'][0]['AvailabilityZone']
            nat['AvailabilityZone'] = az
        if defroute:
            res = ec2_client.replace_route(
                RouteTableId=event['routetableid'],
                DestinationCidrBlock='0.0.0.0/0',
                NatGatewayId=[natid['NatGatewayId'] for natid in natids if natid['AvailabilityZone'] == event['az']][0]
            )
        else:
            res = ec2_client.create_route(
                RouteTableId=event['routetableid'],
                DestinationCidrBlock='0.0.0.0/0',
                NatGatewayId=[natid['NatGatewayId'] for natid in natids if natid['AvailabilityZone'] == event['az']][0]
            )
        log.info(res)
    except Exception as e:
        log.critical(e)
        raise

def main(event, context):
    import cfnresponse
    physical_id = 'RouteChangeResource'
    try:
        log.info('Input event: %s', json.dumps(event))
        requesttype = event['RequestType']
 
        if requesttype == 'Create' and event['ResourceProperties'].get('FailCreate', False):
            raise RuntimeError('Create failure requested')

        success = 0
        for routeinfo in event['ResourceProperties']['RouteMappings']:
            try:
                if requesttype == 'Create' or requesttype == 'Update':
                    defroutetosquid(routeinfo)
                else:
                    defroutetonat(routeinfo)
            except Exception as e:
                success += 1
                continue
        if success:
            raise RuntimeError('Route changes failed')
        attributes = { 'Response': 'Success' }
        cfnresponse.send(event, context, cfnresponse.SUCCESS,
                         attributes, physical_id)
    except Exception as e:
        log.critical(e)
        cfnresponse.send(event, context, cfnresponse.FAILED, {}, physical_id)